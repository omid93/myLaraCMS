<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UserCreateRequest;
use App\Models\Address;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::get();//User::where('wallet','>',100000)->get();

        return view('admin.users.list', compact('users'));
    }

    public function create()
    {
        $userRoles = User::getUserRoles();

        return view('admin.users.create', compact('userRoles'));
    }

    public function store(UserCreateRequest $request)
    {

        $user = User::create([
            'name'     => $request->input('userFullName'),
            'email'    => $request->input('userEmail'),
            'password' => $request->input('userPassword'),
            'role'     => $request->input('userRole')
        ]);
        if ($user && $user instanceof User) {
            return back()->with('status', 'کاربر جدید با موفقیت ایجاد گردید!');
        }
    }

    public function delete(Request $request, $user_id)
    {

        $user         = User::find($user_id);
        $deleteResult = $user->delete();
        if ($deleteResult) {
            return back()->with('status', 'کاربر با موفقیت حذف گردید');
        }
//        User::destroy($user_id);
    }

    public function edit(Request $request, $user_id)
    {

        $user      = User::find($user_id);
        dd( $user->address->getFullState());
        $userRoles = User::getUserRoles();

        return view('admin.users.edit', compact('user', 'userRoles'));

    }

    public function update(Request $request, $user_id)
    {
        $user = User::find($user_id);
        if ($user && $user instanceof User) {
            $userData = [
                'name'  => $request->input('userFullName'),
                'email' => $request->input('userEmail'),
            ];
            if ($request->filled('userPassword')) {
                $userData['password'] = $request->input('userPassword');
            }
            $updateResult = $user->update($userData);
            if ($updateResult) {
                return redirect()->route('admin.users')->with('status', 'کاربر با موفقیت به روز رسانی گردید!');
            }
        }
    }
}
