<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    public $timestamps =  false;

    protected $fillable = ['title'];

    public function categories()
    {
        return $this->hasMany(Category::class,'id');
    }
}
